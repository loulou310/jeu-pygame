import pygame
import math
pygame.init()

# Palette de couleurs

couleurs = [(255,0,0), (0,255,0), (0,0,255), (255,255,255), (148,32,238), (253,142,1), (255,215,0), (0,0,0)]

# constantes du jeu

valeurBoutons = [1, 2, 3, 4, 5, 6]
dessiner = [False] * 6
longueurs = [0] * 6

vitesseVert = 6
vitesseRouge = 5
vitesseBleu = 4
vitesseOrange = 3
vitesseViolet = 2
vitesseOr = 1
score = 0


coutVert = 1
vertObtenu = False
coutManagerVert = 100
coutRouge = 3
rougeObtenu = False
coutManagerRouge = 500
coutBleu = 5
bleuObtenu = False
coutManagerBleu = 1500
coutOrange = 7
orangeObtenu = False
coutManagerOrange = 3000
coutViolet = 10
violetObtenu = False
coutManagerViolet = 7000
coutOr = 15
orObtenu = False
coutManagerOr = 10000

screen = pygame.display.set_mode([340, 460])
pygame.display.set_caption("Clicker")
background = couleurs[3]
framerate = 60
font = pygame.font.Font("selawk.ttf", 16)
timer = pygame.time.Clock()

def draw_task(color, y_coord, value, draw, lenght, speed):
    global score
    if draw and lenght < 260:
        lenght += speed
    elif lenght >= 260:
        draw = False
        lenght = 0
        score += value
    task = pygame.draw.circle(screen, color, (30, y_coord), 20, 5)
    pygame.draw.rect(screen, color, [70, y_coord - 15, 260, 30])
    pygame.draw.rect(screen, couleurs[3], [75, y_coord - 10, 250, 20])
    pygame.draw.rect(screen, color, [70, y_coord - 15, lenght, 30])
    value_text = font.render(str(round(value, 2)), True, couleurs[7])
    screen.blit(value_text, (15, y_coord - 10))

    return task, lenght, draw


def draw_buttons(color, x_coord, cost, owned, manager_cost):
    color_button = pygame.draw.rect(screen, color, [x_coord, 360, 50, 30])
    color_cost = font.render(str(round(cost, 2)), True, couleurs[7])
    screen.blit(color_cost, (x_coord+6, 365))
    if not owned:
        manager_button = pygame.draw.rect(screen, color, [x_coord, 420, 50, 30])
        manager_text = font.render(str(round(manager_cost, 2)), True, couleurs[7])
        screen.blit(manager_text, (x_coord+6, 425))
    else:
        manager_button = pygame.draw.rect(screen, couleurs[3], [x_coord, 420, 50, 30])

    return color_button, manager_button

running = True
while running:
    timer.tick(framerate)
    if vertObtenu and not dessiner[0]:
        dessiner[0] = True
    if rougeObtenu and not dessiner[1]:
        dessiner[1] = True
    if bleuObtenu and not dessiner[2]:
        dessiner[2] = True
    if orangeObtenu and not dessiner[3]:
        dessiner[3] = True
    if violetObtenu and not dessiner[4]:
        dessiner[4] = True
    if orObtenu and not dessiner[5]:
        dessiner[5] = True
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if  event.type == pygame.MOUSEBUTTONDOWN:
            if task1.collidepoint(event.pos):
                dessiner[0] = True
            if task2.collidepoint(event.pos):
                dessiner[1] = True
            if task3.collidepoint(event.pos):
                dessiner[2] = True
            if task4.collidepoint(event.pos):
                dessiner[3] = True
            if task5.collidepoint(event.pos):
                dessiner[4] = True
            if task6.collidepoint(event.pos):
                dessiner[5] = True
            if green_buy.collidepoint(event.pos) and score >= coutVert:
                valeurBoutons[0] += round(math.sqrt(valeurBoutons[0] * 0.1), 2)
                score -= coutVert
                coutVert += round(math.sqrt(coutVert * 1.5), 2)
            if red_buy.collidepoint(event.pos) and score >= coutRouge:
                valeurBoutons[1] += round(math.sqrt(valeurBoutons[1] * 0.1), 2)
                score -= coutRouge
                coutRouge += round(math.sqrt(coutRouge * 1.5), 2)
            if blue_buy.collidepoint(event.pos) and score >= coutBleu:
                valeurBoutons[2] += round(math.sqrt(valeurBoutons[2] * 0.1), 2)
                score -= coutBleu
                coutBleu += round(math.sqrt(coutBleu * 1.5), 2)
            if orange_buy.collidepoint(event.pos) and score >= coutOrange:
                valeurBoutons[3] += round(math.sqrt(valeurBoutons[3] * 0.1), 2)
                score -= coutOrange
                coutOrange += round(math.sqrt(coutOrange * 1.5), 2)
            if purple_buy.collidepoint(event.pos) and score >= coutViolet:
                valeurBoutons[4] += round(math.sqrt(valeurBoutons[4] * 0.1), 2)
                score -= coutViolet
                coutViolet += round(math.sqrt(coutViolet * 1.5), 2)
            if gold_buy.collidepoint(event.pos) and score >= coutOr:
                valeurBoutons[5] += round(math.sqrt(valeurBoutons[5] * 0.1), 2)
                score -= coutOr
                coutOr += round(math.sqrt(coutOr * 1.5), 2)
            if green_manager_buy.collidepoint(event.pos) and score >= coutManagerVert and not vertObtenu:
                vertObtenu = True
                score -= coutManagerVert
            if red_manager_buy.collidepoint(event.pos) and score >= coutManagerRouge and not rougeObtenu:
                rougeObtenu = True
                score -= coutManagerRouge
            if blue_manager_buy.collidepoint(event.pos) and score >= coutManagerBleu and not bleuObtenu:
                bleuObtenu = True
                score -= coutManagerBleu
            if orange_manager_buy.collidepoint(event.pos) and score >= coutManagerOrange and not orangeObtenu:
                orangeObtenu = True
                score -= coutManagerOrange
            if purple_manager_buy.collidepoint(event.pos) and score >= coutManagerViolet and not violetObtenu:
                violetObtenu = True
                score -= coutManagerViolet
            if gold_manager_buy.collidepoint(event.pos) and score >= coutManagerOr and not orObtenu:
                orObtenu = True
                score -= coutManagerOr

    screen.fill(background)
    task1, longueurs[0], dessiner[0] = draw_task(couleurs[1], 50, valeurBoutons[0], dessiner[0], longueurs[0], vitesseVert)
    task2, longueurs[1], dessiner[1] = draw_task(couleurs[0], 100, valeurBoutons[1], dessiner[1], longueurs[1], vitesseRouge)
    task3, longueurs[2], dessiner[2] = draw_task(couleurs[2], 150, valeurBoutons[2], dessiner[2], longueurs[2], vitesseBleu)
    task4, longueurs[3], dessiner[3] = draw_task(couleurs[5], 200, valeurBoutons[3], dessiner[3], longueurs[3], vitesseOrange)
    task5, longueurs[4], dessiner[4] = draw_task(couleurs[4], 250,valeurBoutons[4], dessiner[4], longueurs[4], vitesseViolet)
    task6, longueurs[5], dessiner[5] = draw_task(couleurs[6], 300, valeurBoutons[5], dessiner[5], longueurs[5], vitesseOr)

    green_buy, green_manager_buy = draw_buttons(couleurs[1], 10, coutVert, vertObtenu, coutManagerVert)
    red_buy, red_manager_buy = draw_buttons(couleurs[0], 65, coutRouge, rougeObtenu, coutManagerRouge)
    blue_buy, blue_manager_buy = draw_buttons(couleurs[2], 120, coutBleu, bleuObtenu, coutManagerBleu)
    orange_buy, orange_manager_buy = draw_buttons(couleurs[5], 175, coutOrange, orangeObtenu, coutManagerOrange)
    purple_buy, purple_manager_buy = draw_buttons(couleurs[4], 230, coutViolet, violetObtenu, coutManagerViolet)
    gold_buy, gold_manager_buy = draw_buttons(couleurs[6], 285, coutOr, orObtenu, coutManagerOr)

    afficherScore = font.render('Argent : ' + str(round(score, 2)) +'€', True, couleurs[7])
    screen.blit(afficherScore, (10, 5))

    buyMore = font.render("Acheter plus ?", True, couleurs[7])
    screen.blit(buyMore, (10, 330))

    buyManager = font.render("Acheter des managers ?", True, couleurs[7])
    screen.blit(buyManager, (10, 395))

    pygame.display.flip()

pygame.quit()